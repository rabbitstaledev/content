@extends('layouts.master')

@section('content-header')
    <h1>
        Edit a {{ $posttype->name }} Category
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.content.category.index', [$posttype->slug]) }}">{{ $posttype->name }} Category</a></li>
        <li class="active">Edit a {{ $posttype->name }} Category</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['url' => route('admin.content.category.update', [$posttype->slug, $category->id]), 'method' => 'put']) !!}
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('content::admin.categories.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.content.category.index', [$posttype->slug])}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>

        <div class="col-xs-12 col-md-4">
            <div class="box box-primary">
                <div class="box-body">
                    @if(!$category->isRoot())
                        <div class="form-group">
                            {!! Form::label("parent_id", 'Parent Category') !!}
                            <select class="form-control" name="parent_id">
                                <?php foreach ($rootCategoriesSelectOptions as $key => $val) { ?>
                                <?php
                                    $descendantsArr = $category->descendants()->get()->pluck('id')->toArray();
                                    $selected       = ($key == old('parent_id', $category->parent_id)) ? 'selected="selected"' : '' ;
                                    $disabled       = ($key == $category->id || in_array($key, $descendantsArr)) ? 'disabled="disabled"' : '' ;
                                ?>
                                <option value="{{ $key }}" {{ $selected }} {{ $disabled }}>{{ $val }}</option>
                                <?php } ?>
                            </select>
                        </div>
                    @else
                        <div class="form-group">
                            {!! Form::label("parent_id", 'Parent Category') !!}
                            <select class="form-control" name="parent_id" disabled="disabled">
                                <option value="0">No Parent (Main Category)</option>
                            </select>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@include('content::admin.categories.partials.js-auto-slug')

@section('footer')
<?php /*
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
*/ ?>
@stop
@section('shortcuts')
<?php /*
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
*/ ?>
@stop

@push('js-stack')
<?php /*
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.content.category.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
*/ ?>
@endpush
