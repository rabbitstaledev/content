<div class="box-body">
    {!! Form::i18nInput('title', 'Title', $errors, $lang, old("$lang.title", $category), ['autocomplete' => 'off']) !!}

    <div class="form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}">
        <label for="{{ $lang }}[slug]">Slug</label>
        <div class="input-group">
            <div class="input-group-addon">
                {{ url("/content/{$posttype->slug}/category") }}/
            </div>
            <input type="text" class="form-control" id="{{ $lang }}[slug]" name="{{ $lang }}[slug]" autocomplete="off" value="{{ old("$lang.slug", $category->translateOrNew($lang)->slug) }}">
        </div>
        {!! $errors->first("{$lang}.slug", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("{$lang}.detail") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[detail]", 'Detail') !!}
        {!! Form::textarea("{$lang}[detail]", old("{$lang}[detail]", $category->translateOrNew($lang)->detail), ['class' => 'form-control', 'placeholder' => 'Detail']) !!}
        {!! $errors->first("{$lang}.detail", '<span class="help-block">:message</span>') !!}
    </div>

</div>
