@extends('layouts.master')

@section('content-header')
    <h1>
        {{ str_plural($posttype->name) }} Category
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ $posttype->name }} Category</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.content.category.create', [$posttype->slug]) }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> Create a {{ $posttype->name }} Category
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th class="text-center" colspan="2" width="240" data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($categories) && !$categories->isEmpty()): ?>
                            <?php foreach ($categories as $category): ?>
                            <tr>
                                <td>
                                    {{ str_repeat('----- ', $category->depth) }} {{ $category->title }}
                                </td>
                                <td>{{ $category->slug }}</td>
                                <td>
                                    <div class="btn-group">
                                        <?php if ($category->depth == 0) { ?>
                                        <a href="{{ route('admin.content.category.create', [$posttype->slug]) }}?parent_id={{ $category->id }}" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Add Child</a>
                                        <?php } ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.content.category.edit', [$posttype->slug, $category->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.content.category.destroy', [$posttype->slug, $category->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th width="150">&nbsp;</th>
                                <th width="75">&nbsp;</th>
                                <th width="120">&nbsp;</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
<?php /*
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
*/ ?>
@stop
@section('shortcuts')
<?php /*
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('content::categories.title.create category') }}</dd>
    </dl>
*/ ?>
@stop

@push('js-stack')
<?php /*
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.content.category.create') ?>" }
                ]
            });
        });
    </script>
*/ ?>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": false,
                "info": true,
                "autoWidth": true,
                // "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
