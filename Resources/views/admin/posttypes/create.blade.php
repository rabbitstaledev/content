@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('content::posttypes.title.create posttype') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.content.posttype.index') }}">{{ trans('content::posttypes.title.posttypes') }}</a></li>
        <li class="active">{{ trans('content::posttypes.title.create posttype') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.content.posttype.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <div class="box-body">
                    {!! Form::normalInput('name', 'Name', $errors, old("name"), ['autocomplete' => 'off']) !!}

                    {!! Form::normalInput('slug', 'Slug', $errors, old("slug"), ['autocomplete' => 'off']) !!}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.content.posttype.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@push('js-stack')
<script>
$(function(){
    $('input[name="name"]').keyup(function(){
        var slugifyTxt = $(this).val().toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');
        $('input[name="slug"]').val( slugifyTxt );
    });
});
</script>
@endpush

@section('footer')
<?php /*
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
*/ ?>
@stop
@section('shortcuts')
<?php /*
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
*/ ?>
@stop

@push('js-stack')
<?php /*
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.content.posttype.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
*/ ?>
@endpush
