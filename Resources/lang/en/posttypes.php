<?php

return [
    'list resource' => 'List posttypes',
    'create resource' => 'Create posttypes',
    'edit resource' => 'Edit posttypes',
    'destroy resource' => 'Destroy posttypes',
    'title' => [
        'posttypes' => 'Posttype',
        'create posttype' => 'Create a posttype',
        'edit posttype' => 'Edit a posttype',
    ],
    'button' => [
        'create posttype' => 'Create a posttype',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
