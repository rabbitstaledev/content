<?php

$langCategoryArr = [
    'list resource' => 'List categories',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Destroy categories',
    'title' => [
        'categories' => 'Category',
        'create category' => 'Create a category',
        'edit category' => 'Edit a category',
    ],
    'button' => [
        'create category' => 'Create a category',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];

$posttypes = \Modules\Content\Entities\Posttype::get();
// $posttypes = app('ContentPosttypesCollection');

foreach ($posttypes as $posttype) {
    $langCategoryArr["{$posttype->slug} list resource"] = "List {$posttype->name} categories";
    $langCategoryArr["{$posttype->slug} create resource"] = "Create {$posttype->name} categories";
    $langCategoryArr["{$posttype->slug} edit resource"] = "Edit {$posttype->name} categories";
    $langCategoryArr["{$posttype->slug} destroy resource"] = "Destroy {$posttype->name} categories";
}

return $langCategoryArr;