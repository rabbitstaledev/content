<?php

namespace Modules\Content\Entities;

// use Dimsav\Translatable\Translatable;
use Modules\Content\Entities\Traits\CategoryTranslatable;

class Category extends \Baum\Node
{
    // use Translatable;
    use CategoryTranslatable;

    protected $scoped = ['posttype_id'];

    protected $table = 'content__categories';
    public $translatedAttributes = ['title', 'slug', 'detail'];
    protected $fillable = ['parent_id', 'posttype_id', 'title', 'slug', 'detail'];

    // ========== Relations ==========
    public function posttype()
    {
        return $this->belongsTo(\Modules\Content\Entities\Posttype::class);
    }

    public function posts()
    {
        return $this->belongsToMany(\Modules\Content\Entities\Post::class, 'content__post_category');
    }

    // ========== Getters & Setters ==========
    public function getPosttypeSlugAttribute()
    {
        return $this->posttype->slug;
    }

    public function getPermalinkAttribute()
    {
        return route('content.category.index', [$this->posttypeSlug, $this->slug]);
    }

    // ========== Other Utilities ==========
    public function isPosttype($posttype)
    {
        if (!$posttype instanceof Posttype) {
            $posttypeRepo = app('Modules\Content\Repositories\PosttypeRepository');
            $posttype = $posttypeRepo->findBySlug($posttype);
        }

        if (!empty($posttype) && !empty($posttype->getKey())) {
            if ($posttype->id == $this->posttype_id) {
                return true;
            }
        }

        return false;
    }
}
