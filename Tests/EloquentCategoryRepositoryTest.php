<?php

namespace Modules\Content\Tests;

class EloquentCategoryRepositoryTest extends BaseContentTestCase
{
    /** @test */
    public function it_can_make_children_category()
    {
        // Prepare Blog Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);

        // Create 2 ParentCatBlog, 3 ChildrenCatBlog and 1 Child of Children
        // method createCategoryInPosttype can parse 2 parameters, 
        // first is posttype and second is parentId
        $parentCat1 = $this->createCategoryInPosttype($posttypeBlog);
        $parentCat2 = $this->createCategoryInPosttype($posttypeBlog);
        $childrenCat1_1 = $this->createCategoryInPosttype($posttypeBlog, $parentCat1->id);
        $childrenCat1_2 = $this->createCategoryInPosttype($posttypeBlog, $parentCat1->id);
        $childrenCat2_1 = $this->createCategoryInPosttype($posttypeBlog, $parentCat2->id);
        $childrenCat1_2_1 = $this->createCategoryInPosttype($posttypeBlog, $childrenCat1_2->id);

        $this->assertEquals($childrenCat1_1->parent_id, $parentCat1->id);
        $this->assertEquals($childrenCat1_2->parent_id, $parentCat1->id);
        $this->assertEquals($childrenCat2_1->parent_id, $parentCat2->id);
        $this->assertEquals($childrenCat1_2_1->parent_id, $childrenCat1_2->id);
        $this->assertEquals($parentCat1->depth, 0);
        $this->assertEquals($parentCat2->depth, 0);
        $this->assertEquals($childrenCat1_1->depth, 1);
        $this->assertEquals($childrenCat1_2->depth, 1);
        $this->assertEquals($childrenCat2_1->depth, 1);
        $this->assertEquals($childrenCat1_2_1->depth, 2);
    }

    /** @test */
    public function it_can_get_categories_by_each_posttype()
    {
        // Prepare Blog and News Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);
        $posttypeNews = $this->createPosttype(['name' => 'News', 'slug' => 'news']);

        // Create 2 ParentCatBlog and 5 ChildrenCatBlog
        $parentCatBlog1 = $this->createCategoryInPosttype($posttypeBlog);
        $parentCatBlog2 = $this->createCategoryInPosttype($posttypeBlog);
        $childrenCatBlog1_1 = $this->createCategoryInPosttype($posttypeBlog, $parentCatBlog1);
        $childrenCatBlog1_2 = $this->createCategoryInPosttype($posttypeBlog, $parentCatBlog1);
        $childrenCatBlog2_1 = $this->createCategoryInPosttype($posttypeBlog, $parentCatBlog2);
        $childrenCatBlog2_2 = $this->createCategoryInPosttype($posttypeBlog, $parentCatBlog2);
        $childrenCatBlog1_3 = $this->createCategoryInPosttype($posttypeBlog, $parentCatBlog1);

        // Create 2 ParentNewsBlog and 3 ChildrenNewsBlog
        $parentCatNews1 = $this->createCategoryInPosttype($posttypeNews);
        $parentCatNews2 = $this->createCategoryInPosttype($posttypeNews);
        $childrenCatNews1_1 = $this->createCategoryInPosttype($posttypeNews, $parentCatNews1);
        $childrenCatNews2_1 = $this->createCategoryInPosttype($posttypeNews, $parentCatNews2);
        $childrenCatNews2_2 = $this->createCategoryInPosttype($posttypeNews, $parentCatNews2);

        // Test - Get Post By Posttype
        $blogCategories = $this->category->getByPosttype('blog');
        $newsCategories = $this->category->getByPosttype('news');

        $this->assertEquals(7, $blogCategories->count());
        $this->assertEquals(5, $newsCategories->count());
    }
}