<?php

namespace Modules\Content\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CategoryApiTransformer extends Resource
{
    public function toArray($request)
    {
        $category = $this->resource;
        $category->setAppends(['permalink']);
        $responseArr = array_only($category->toArray(), ['id', 'parent_id', 'title', 'slug', 'detail', 'permalink']);
        // $responseArr = $category->toArray();
        $category->load(['parent', 'children']);
        if (!empty($category->parent)) {
            $responseArr['parent'] = array_only($category->parent->toArray(), ['id', 'parent_id', 'title', 'slug', 'detail']) ;
        } else {
            $responseArr['parent'] = new \stdClass();
        }

        if (!$category->children->isEmpty()) {
            $responseArr['children'] = CategoryHierarchyApiTransformer::collection($category->children);
        } else {
            $responseArr['children'] = [];
        }

        return $responseArr;
    }
}
