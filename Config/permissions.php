<?php

$permissionsArr = [
    // 'content.posts' => [
    //     'index' => 'content::posts.list resource',
    //     'create' => 'content::posts.create resource',
    //     'edit' => 'content::posts.edit resource',
    //     'destroy' => 'content::posts.destroy resource',
    // ],
    // 'content.categories' => [
    //     'index' => 'content::categories.list resource',
    //     'create' => 'content::categories.create resource',
    //     'edit' => 'content::categories.edit resource',
    //     'destroy' => 'content::categories.destroy resource',
    // ],
    'content.posttypes' => [
        'index' => 'content::posttypes.list resource',
        'create' => 'content::posttypes.create resource',
        'edit' => 'content::posttypes.edit resource',
        'destroy' => 'content::posttypes.destroy resource',
    ],
// append


];

try {
    $posttypes = \Modules\Content\Entities\Posttype::get();
    // $posttypes = app('ContentPosttypesCollection');

    foreach ($posttypes as $posttype) {
        $permissionsArr["content.{$posttype->slug}.posts"] = [
            'index'   => "content::posts.{$posttype->slug} list resource",
            'create'  => "content::posts.{$posttype->slug} create resource",
            'edit'    => "content::posts.{$posttype->slug} edit resource",
            'destroy' => "content::posts.{$posttype->slug} destroy resource",
        ];
        $permissionsArr["content.{$posttype->slug}.categories"] = [
            'index'   => "content::categories.{$posttype->slug} list resource",
            'create'  => "content::categories.{$posttype->slug} create resource",
            'edit'    => "content::categories.{$posttype->slug} edit resource",
            'destroy' => "content::categories.{$posttype->slug} destroy resource",
        ];
    }
}
catch (\Exception $e) {
    // table not found ...
    //
    //
}

return $permissionsArr;