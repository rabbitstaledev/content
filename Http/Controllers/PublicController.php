<?php

namespace Modules\Content\Http\Controllers;

use Modules\Content\Repositories\CategoryRepository;
use Modules\Content\Repositories\PostRepository;
use Modules\Tag\Repositories\TagRepository;
use Modules\Core\Http\Controllers\BasePublicController;

use Modules\Content\Entities\Category;
use Modules\Content\Entities\Post;
use Modules\Content\Entities\Posttype;

class PublicController extends BasePublicController
{
    private $post, $category;

    public function __construct(PostRepository $post, CategoryRepository $category, TagRepository $tag)
    {
        parent::__construct();

        $this->post = $post;
        $this->category = $category;
        $this->tag = $tag;
    }

    public function postIndex(Posttype $postTypeObj)
    {
        $data = [];
        $posttype = $postTypeObj;
        $defaultPostsPerPage = 10;

        $params = [
            'page'           => request()->input('page', 1),
            'perPage'        => request()->input('perPage', $defaultPostsPerPage),
            'orderBy'        => request()->input('orderBy', 'id'),
            'sortOrder'      => request()->input('sortOrder', 'desc'),
            // 'categoryId'     => request()->input('categoryId'),
            'posttype'       => $posttype,
            'status'         => 1,
            'onScheduleOnly' => true,
        ];
        $posts = $this->post->search($params);

        $data['pageTitle'] = $posttype->name . ' Posts';
        $data['posts'] = $posts;
        $data['posttype'] = $posttype;

        return view('content.post-index', $data);
    }

    public function categoryIndex(Posttype $postTypeObj, Category $postCategoryObj)
    {
        $data = [];
        $posttype = $postTypeObj;
        $category = $postCategoryObj;
        $defaultPostsPerPage = 10;

        // s($category->permalink); die;

        $params = [
            'page'           => request()->input('page', 1),
            'perPage'        => request()->input('perPage', $defaultPostsPerPage),
            'orderBy'        => request()->input('orderBy', 'id'),
            'sortOrder'      => request()->input('sortOrder', 'desc'),
            'categoryId'     => $category->id,
            'posttype'       => $posttype,
            'status'         => 1,
            'onScheduleOnly' => true,
        ];
        $posts = $this->post->search($params);

        $data['pageTitle'] = $category->title . ' ' . $posttype->name . ' Posts';
        $data['category'] = $category;
        $data['posts'] = $posts;
        $data['posttype'] = $posttype;

        return view('content.post-index', $data);
    }

    public function tagIndex(Posttype $postTypeObj, $slug)
    {
        // s($slug); die;
        $data = [];
        $posttype = $postTypeObj;
        $defaultPostsPerPage = 10;

        $params = [
            'page'           => request()->input('page', 1),
            'perPage'        => request()->input('perPage', $defaultPostsPerPage),
            'orderBy'        => request()->input('orderBy', 'id'),
            'sortOrder'      => request()->input('sortOrder', 'desc'),
            'posttype'       => $posttype,
            'tags'           => $slug,
            'status'         => 1,
            'onScheduleOnly' => true,
        ];
        $posts = $this->post->search($params);

        $tag = $this->tag->findBySlug($slug);
        $tagTitle = (empty($tag)) ? $slug : $tag->name ;

        $data['pageTitle'] = "Tag : {$tagTitle}";
        $data['posts'] = $posts;
        $data['posttype'] = $posttype;

        return view('content.post-index', $data);
    }

    public function postShow(Posttype $postTypeObj, Post $post)
    {
        $data = [];
        $data['post'] = $post;

        return view('content.post-show', $data);
    }
}