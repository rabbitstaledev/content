<?php

namespace Modules\Content\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Content\Entities\Category;
use Modules\Content\Entities\Posttype;
use Modules\Content\Http\Requests\CreateCategoryRequest;
use Modules\Content\Http\Requests\UpdateCategoryRequest;
use Modules\Content\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class CategoryController extends AdminBaseController
{
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(CategoryRepository $category)
    {
        parent::__construct();

        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Posttype $postTypeObj)
    {
        $posttype = $postTypeObj;
        $categories = $this->category->getByPosttype($postTypeObj->slug);

        return view('content::admin.categories.index', compact('categories', 'posttype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Posttype $postTypeObj)
    {
        $data = [];
        $posttype = $postTypeObj;
        $data['posttype'] = $posttype;

        $rootCategories = Category::roots()->where('posttype_id', $posttype->id)->get();
        $rootCategoriesSelectOptions = [];
        $rootCategoriesSelectOptions[0] = 'No Parent (Main Category)';
        $rootCategoriesSelectOptions = $rootCategoriesSelectOptions + $rootCategories->pluck('title', 'id')->toArray();
        $data['rootCategoriesSelectOptions'] = $rootCategoriesSelectOptions;

        return view('content::admin.categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest $request
     * @return Response
     */
    public function store(Posttype $postTypeObj, CreateCategoryRequest $request)
    {
        // \DB::enableQueryLog();

        $input = $request->all();
        $input['posttype_id'] = $postTypeObj->id;

        if ($input['parent_id'] == 0) {
            unset($input['parent_id']);
        }

        $this->category->create($input);

        // $logs = \DB::getQueryLog();
        // s($logs);
        // die;

        return redirect()->route('admin.content.category.index', [$postTypeObj->slug])
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('content::categories.title.categories')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return Response
     */
    public function edit(Posttype $postTypeObj, Category $category)
    {
        $data = [];

        $posttype = $postTypeObj;
        if ($postTypeObj->id != $category->posttype_id) {
            abort(404);
        }

        $data['posttype'] = $posttype;
        $data['category'] = $category;

        $rootCategories = Category::roots()->where('posttype_id', $posttype->id)->get();
        $rootCategoriesSelectOptions = [];
        $rootCategoriesSelectOptions[0] = 'No Parent (Main Category)';
        $rootCategoriesSelectOptions = $rootCategoriesSelectOptions + $rootCategories->pluck('title', 'id')->toArray();
        $data['rootCategoriesSelectOptions'] = $rootCategoriesSelectOptions;

        return view('content::admin.categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     * @param  UpdateCategoryRequest $request
     * @return Response
     */
    public function update(Posttype $postTypeObj, Category $category, UpdateCategoryRequest $request)
    {
        if ($postTypeObj->id != $category->posttype_id) {
            abort(404);
        }
        $this->category->update($category, $request->all());

        return redirect()->route('admin.content.category.index', [$postTypeObj->slug])
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('content::categories.title.categories')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function destroy(Posttype $postTypeObj, Category $category)
    {
        if ($postTypeObj->id != $category->posttype_id) {
            abort(404);
        }
        $this->category->destroy($category);

        return redirect()->route('admin.content.category.index', [$postTypeObj->slug])
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('content::categories.title.categories')]));
    }
}
