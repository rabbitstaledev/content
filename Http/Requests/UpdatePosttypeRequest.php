<?php

namespace Modules\Content\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePosttypeRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => [
                'required',
                \Illuminate\Validation\Rule::unique('content__posttypes')->ignore($this->posttype->id),
            ],
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
