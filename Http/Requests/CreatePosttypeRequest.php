<?php

namespace Modules\Content\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreatePosttypeRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required|unique:content__posttypes',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
