<?php

namespace Modules\Content\Repositories\Eloquent;

use Modules\Content\Repositories\PosttypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPosttypeRepository extends EloquentBaseRepository implements PosttypeRepository
{
    public function all()
    {
        // if (method_exists($this->model, 'translations')) {
        //     return $this->model->with('translations')->orderBy('created_at', 'DESC')->get();
        // }

        return $this->model->orderBy('id', 'ASC')->get();
    }

    // public function findByAttributes($arr)
    // {
    //     s('xxx'); die;
    // }
}
