# Content Module (AsgardCMS)
นี่คือ Content Management Module สำหรับโปรเจคที่ใช้งาน AsgardCMS เป็น Core ของโปรเจค

## Installation
คุณสามารถเลือกวิธีการ install โดยพิจารณาตามความเหมาะสมในการใช้งานของคุณ ได้ดังนี้

### A. Install by Composer
กรณีที่สามารถนำ Module ไปใช้ทันที โดยไม่ต้องการแก้ไขอะไรเพิ่มเติม สามารถ Install Module ผ่าน Composer ได้ทันที โดยให้เราเพิ่มรายละเอียดในไฟล์ composer.json ของโปรเจคดังนี้

    {
        "require": {
            "cnc/content": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "https://bitbucket.org/rabbitstaledev/content.git"
            }
        ],
        "extra": {
            "laravel": {
                "dont-discover": [
                    "cnc/content"
                ]
            }
        }
    }


### B. Git Fork and Install by Composer
**หากเป็นโปรเจคที่มีความต้องการที่จะ Customized Module เพิ่มเติม** ขอแนะนำให้ทำ Git Fork ออกไปสร้าง Repository ใหม่สำหรับใช้งานในโปรเจคนั้นเอง และแก้ไขชื่อ Package และ URL ของ Repo ในไฟล์ composer.json ให้มีรายละเอียดเป็นตามที่ Fork แยกออกมา

    {
        "require": {
            "your-workbench/content": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "{{ your-git-repo-url }}"
            }
        ],
        "extra": {
            "laravel": {
                "dont-discover": [
                    "your-workbench/content"
                ]
            }
        }
    }

### C. Install by Download Module
**หมายเหตุ - วิธีนี้แนะนำให้ทำได้แค่กรณีเดียวเท่านั้นคือ คุณต้องการมี Git Repo ของโปรเจคแค่เพียงตัวเดียว ที่มีทั้ง CorePlatform ของ AsgardCMS และ Modules ทั้งหมดอยู่รวมด้วยกัน (ไม่ได้แยกกัน Dev แต่ละ Module ออกจากกันอย่างอิสระ)**

หากเป็นโปรเจคที่ต้องการ Customized Module เพิ่มเติม แต่ไม่ต้องการจัดการ Module ผ่าน Composer คุณสามารถ Download Module นี้ และนำไปวางใน Folder ของ AsgardCMS Project ได้ทันที ที่ Folder **Modules/Content**

จากนั้นให้สั่ง run command เพื่อ install dependency package ที่จำเป็นต้องใช้สำหรับ Content Module ดังนี้
```
composer require baum/baum
```

## Migrations & Configurations
หลังจาก Install สำเร็จแล้ว ให้ Run คำสั่งสำหรับ Migrations และคำสั่งสำหรับ Publish Backend Assets ดังนี้
```
php artisan module:migrate Content
php artisan module:publish Content
```

จากนั้น ให้เข้าไป Allow Permission สำหรับใช้งาน Content Module ของ User ในแต่ละ Role ที่หน้า backend/user/roles

## Feature & Usage

### Backend
Backend จะประกอบไปด้วยสองส่วนคือ Posttype Management และ Post & Category Management

#### Posttype Management
คุณสามารถเข้าไปยังหน้า backend/content/posttype เพื่อจัดการสร้าง Posttype แต่ละชนิด ตามที่เว็บไซต์ของโปรเจคคุณต้องการใช้งาน 
เมื่อสร้าง Posttype สำเร็จแล้ว ก็จะมีเมนูสำหรับจัดการ CRUD Post และ CRUD Category สำหรับ Posttype นั้นๆ ได้

#### Post Management & Category Management
ใน Post และ Category ของแต่ละ Posttype จะประกอบด้วย Feature ดังนี้

##### Post
- Post สามารถเขียนรายละเอียดของ Title, Slug, ShortDetail และ Detail โดยสนับสนุนกับระบบ Translation ได้
- Post สามารถตั้งสถานะ (Status) เป็น Publish/Draft ได้
- Post สามารถตั้งช่วงเวลาในการแสดงผลที่ Frontend ได้ ที่ field start_date และ end_date
- Post สามารถเลือกอยู่ได้กับ Category หลายหมวดหมู่ได้ (Many-to-Many Relations between Post and Category)
- Post สามารถใช้งาน Tag ได้ (ใช้งานร่วมกับ Tag Module ของ AsgardCMS)
- Post สามารถใช้งาน Media ได้ (ใช้งานร่วมกับ Media Module ของ AsgardCMS)

##### Category
- Category สามารถเขียนรายละเอียดของ Title, Slug และ Detail โดยสนับสนุนกับระบบ Translation ได้
- Category สามารถเลือกสร้างเป็นลำดับชั้น (Parent/Children) โดยจะสามารถมี Children Category ได้ 1 Level

### API
Content API จะเป็น Public API สำหรับข้อมูลของ Post และ Category ของแต่ละ Posttype ให้ Frontend นำไปใช้งานได้
คุณสามารถดูรายละเอียด document ของ API ได้ โดยการใช้คำสั่งของ APIDoc ดังนี้ 
```
 apidoc -i Modules -f ".*\\.php" -o public/path-to-apidoc
```
หลังจากสร้าง document แล้ว คุณสามารถดูได้ที่
```
http://{your-domain}/path-to-apidoc
```

### Frontend Route & Controller
มีตัวอย่างการใช้งาน Frontend Route และ Controller แล้ว คุณสามารถแก้ไขส่วนนี้ได้ด้วยตนเองตามที่ต้องการ

## Developer Note
### Repositories
Content Module นี้ใช้ Concept ของ Repository Pattern ในการจัดการข้อมูลกับ Database
โดยใน Repository แต่ละตัว จะมี Helpers Method ช่วยอยู่หลายอย่าง เช่น method create, update, find, findWhere, findBySlug, etc.
คุณสามารถเข้าไปดูรายละเอียดของ method เหล่านี้ได้จากโค้ดแต่ละไฟล์

#### PostRepository
ใน PostRepository จะมี Method สำหรับใช้ search Post อยู่ โดยจะ Return ผลลัพท์ออกมาเป็น Paginate Collection 

Method search นี้ ได้มีการนำมาใช้งานใน Content Module ทั้งในส่วนของ BackendController, FrontendController และ ApiController

หากมีส่วนไหนที่คุณต้องทำงานที่เกี่ยวข้องกับการ query ค้นหาข้อมูลของ Post ขอแนะนำให้ใช้ Method search นี้เช่นเดียวกัน

โดย method search นั้นรองรับ parameters ดังนี้ (ดูรายละเอียดจากส่วนหนึ่งของโค้ดใน method search ด้านล่าง)

    // ไฟล์ Modules/Content/Repositories/Eloquent/EloquentPostRepository.php
    public function search($params = [])
    {
        $defaultParams = [
            'page'           => 1, // หน้าของผลลัพท์การค้นหา
            'perPage'        => -1, // จำนวนผลลัพท์ต่อหนึ่งหน้า (-1 คือ ต้องการผลลัพท์ทั้งหมด)
            'orderBy'        => 'id', // เรียงลำดับผลลัพท์ตามฟิลด์
            'sortOrder'      => 'asc', // เรียงลำดับแบบ asc/desc
            'posttype'       => null, // ชนิดของ posttype ที่ต้องการ filter
            'categoryId'     => null, // filter by categoryId
            'tags'            => null, // filter by tag slug
            'status'         => array_get($params, 'status', -1), // filter by status (-1 คือ ต้องการผลลัพท์ทุก status)
            'onScheduleOnly' => false, // จะเลือกดึงเฉพาะผลลัพท์ที่แสดงผลในช่วงเวลาปัจจุบันเท่านั้นหรือไม่
        ];
        $params = array_merge($defaultParams, array_filter($params));
        
        // ...... (โค้ดส่วนอื่นๆ สำหรับใช้ filter search results) ......
        // ......
        // ......
    }

### Unit Test
Content Module ได้มีการเตรียม Unit Test ส่วนหนึ่งสำหรับทดสอบ Method ใน Repository แล้ว สามารถดูตัวอย่างการเขียน Unit Test ได้จากไฟล์ใน folder Modules/Content/Tests

การ Run Command สำหรับ UnitTest เราจะใช้คำสั่งของ PHPUnit ดังนี้
```
./vendor/bin/phpunit --bootstrap vendor/autoload.php Modules/Content
```

หากต้องการแสดงรายละเอียดของแต่ละ TestCase ให้เพิ่ม parameter --testdox เข้ามา
```
./vendor/bin/phpunit --bootstrap vendor/autoload.php Modules/Content --testdox
```

## TODO
สิ่งที่จะทำเพิ่มในอนาคต
...
...

## Others
หากมีข้อสงสัย หรือคำแนะนำเพิ่มเติม ติดต่อสอบถามได้นะครับ